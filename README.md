-------------------------------------------
**User Interface (UI) Helper Library**
--------------------------------------------
* Author: Justin Robert Evans
* Version: 0.1.1
  
Implements functionality for HTML5 elements, Client-Side GUI components, Client-Side Database Interface (TBD), Form extensions (done), etc.
  
05/07/2015

--------------------------------------------
**Base Modules:**
--------------------------------------------
Forms - DOM/UX/UI/LocalStorage Functionality 

DB  - To be implemented - Adapters and
      interfaces for Web SQL, IndexedDB and
      "localStorage" for supported browsers 
      (maybe, depends on my **mood** and, of 
      course, a good reason *why*; local storage
      seems to be enough for now.)

```
var me = { time : 365 * 24 * 60, mood : 10 };
```

```
console.log( 'Possible? ' 
             + !! (function(my) { 
                       return ( my.time > 0 && my.mood-- >= 0 ) 
                                  ? --my.time 
                                  : 0; 
                   })(me) 
           );
```

--------------------------------------------
Requirements (TODO: Define in Module Deps):
--------------------------------------------
  * jQuery      v 1.10.2
  * Typeahead   v 0.10.5
  * Handlebars  v 3.0.1
  * TinyMCE     v 4.0.5
  * typeahead.bundle.js::Bloodhound() [ https://github.com/twitter/typeahead.js/ ]

--------------------------------------------
Overall TODOs:
--------------------------------------------
  * Webpack integration
  * Karma/Jasmine tests
  * Swap TinyMCE with CKEditor (possibly - this
    is still in deliberation.)

Tests, documentation, additional functionality and demos possibly coming *some* **time** in the future.

```
console.log( 'Really? ' + !! (function(my){ return --my.time; })(me) );
```